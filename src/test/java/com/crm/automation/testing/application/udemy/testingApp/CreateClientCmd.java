package com.crm.automation.testing.application.udemy.testingApp;

import com.crm.automation.testing.application.udemy.commons.Gender;
import com.crm.automation.testing.application.udemy.commons.commonType;
import com.crm.automation.testing.application.udemy.input.ClienteInput;
import com.crm.automation.testing.application.udemy.model.Client;
import com.crm.automation.testing.application.udemy.tools.StringGenerator;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.jayway.restassured.response.Response;
import lombok.Getter;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.util.Date;

import static com.jayway.restassured.RestAssured.given;

/**
 * @Autor Henry Joseph Calani A.
 **/

@SynchronousExecution
public class CreateClientCmd implements BusinessLogicCommand {
    private ClienteInput input;

    @Autowired
    private StringGenerator stringGenerator;

    @Getter
    private Client instanClient;

    @Getter
    String result;

    @Getter
    private  Integer clientId;

    @Override
    public void execute() {

       Date date = new Date();
       Timestamp timestamp = new Timestamp(date.getTime());

       input = new ClienteInput();
       input.setFirstName(stringGenerator.next());
       input.setLastName(stringGenerator.next());
       input.setLastPurchase(timestamp);
       input.setDeleted(Boolean.TRUE);
       input.setGender(Gender.MALE);
       input.setEmail(stringGenerator.next()+ "@gmail.com");


        Response response =  given ()
                .contentType("application/json")
                .body(input)
                .when()
                .then()
                .log().ifError()
                .statusCode(200)
                .post(commonType.URL_PROYECT.getKey() + "/client");

        Assert.assertFalse(response.asString().contains("isError"));
        result = response.asString();
        Assert.assertNotNull(result);

        instanClient = response.as(Client.class);
        clientId = Math.toIntExact(instanClient.getId());
        Assert.assertNotNull(instanClient);
    }
}
