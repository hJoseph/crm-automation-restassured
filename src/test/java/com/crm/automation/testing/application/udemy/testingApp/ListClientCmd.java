package com.crm.automation.testing.application.udemy.testingApp;

import com.crm.automation.testing.application.udemy.commons.Gender;
import com.crm.automation.testing.application.udemy.commons.commonType;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.jayway.restassured.response.Response;
import lombok.Getter;
import org.junit.Assert;

import static com.jayway.restassured.RestAssured.given;

/**
 * @Autor Henry Joseph Calani A.
 **/
@SynchronousExecution
public class ListClientCmd implements BusinessLogicCommand {
    @Getter
    private  String result;

    @Override
    public void execute() {
        Response response =  given ()
                .contentType("application/json")
                .when()
                .then()
                .log().ifError()
                .statusCode(200)
                .get(commonType.URL_PROYECT.getKey()+"/client/list?gender="+ Gender.MALE);
        Assert.assertFalse(response.asString().contains("isError"));
        result = response.asString();
    }
}
