package com.crm.automation.testing.application.udemy.testingApp;

import com.crm.automation.testing.application.udemy.AbstractTest;
import com.crm.automation.testing.application.udemy.annotations.ExecutionLevel;
import com.crm.automation.testing.application.udemy.annotations.ExecutionPriority;
import com.crm.automation.testing.application.udemy.commons.commonType;
import com.jayway.restassured.response.Response;
import io.restassured.http.ContentType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static com.jayway.restassured.RestAssured.given;

/**
 * @Autor Henry Joseph Calani A.
 **/
@ExecutionPriority(ExecutionLevel.FIRST_DETAIL)
public class DeleteEmployeeTest extends AbstractTest {
    @Autowired
    private  CreateEmployeeCmd createEmployeeCmd;

    private  Integer personId;

    @Before
    public void createEmployee(){
        createEmployeeCmd.execute();
        String employee = createEmployeeCmd.getResult();
        personId = createEmployeeCmd.getPersonId();
        Assert.assertNotNull(employee);
    }

    @Test
    public void deleteEmployee(){

        Response response =  given ()
                .contentType(String.valueOf(ContentType.JSON))
                .when()
                .then()
                .log().ifError()
                .statusCode(200)
                .delete(commonType.URL_PROYECT.getKey()+"/client/"+personId);
        Assert.assertFalse(response.asString().contains("isError"));

    }

}
