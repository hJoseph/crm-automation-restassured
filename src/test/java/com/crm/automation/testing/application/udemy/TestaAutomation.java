package com.crm.automation.testing.application.udemy;

import com.crm.automation.testing.application.udemy.tools.StringGenerator;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Autor Henry Joseph Calani A.
 **/
public class TestaAutomation extends  AbstractTest {
    @Autowired
    private StringGenerator stringGenerator;

    @Test
    public void testAutomation(){
        String value = stringGenerator.next();
        System.out.print("this is value" + value);

    }
}
