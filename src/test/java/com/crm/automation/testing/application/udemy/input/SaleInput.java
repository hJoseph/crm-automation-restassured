package com.crm.automation.testing.application.udemy.input;

import lombok.Data;

import java.util.Date;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Data
public class SaleInput {
    private Long id;
    private Long numberSale;
    private Date icreatedDate;
}
