package com.crm.automation.testing.application.udemy;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @Autor Henry Joseph Calani A.
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = {
                com.crm.automation.testing.application.udemy.Config.class
        }
)
public abstract class AbstractTest {
}
