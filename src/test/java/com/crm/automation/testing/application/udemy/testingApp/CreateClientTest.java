package com.crm.automation.testing.application.udemy.testingApp;

import com.crm.automation.testing.application.udemy.AbstractTest;
import com.crm.automation.testing.application.udemy.annotations.ExecutionLevel;
import com.crm.automation.testing.application.udemy.annotations.ExecutionPriority;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Autor Henry Joseph Calani A.
 **/
@ExecutionPriority(ExecutionLevel.FIRST_MASTER)
public class CreateClientTest extends AbstractTest {

    @Autowired
    private  CreateClientCmd createClientCmd;

    @Test
    public  void createClient(){
       createClientCmd.execute();
       String clientResult = createClientCmd.getResult();
       Assert.assertNotNull(clientResult);
    }
}
