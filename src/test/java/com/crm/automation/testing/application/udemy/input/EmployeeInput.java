package com.crm.automation.testing.application.udemy.input;

import lombok.Data;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Data
public class EmployeeInput {

    private String position;

    private String email;

    private String firstName;

    private String lastName;

    private Boolean isDeleted;
}
