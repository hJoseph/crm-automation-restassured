package com.crm.automation.testing.application.udemy.testingApp;

import com.crm.automation.testing.application.udemy.AbstractTest;
import com.crm.automation.testing.application.udemy.annotations.ExecutionLevel;
import com.crm.automation.testing.application.udemy.annotations.ExecutionPriority;
import lombok.Getter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Autor Henry Joseph Calani A.
 **/
@ExecutionPriority(ExecutionLevel.SECOND_MASTER)
public class UpdateClientTest extends AbstractTest {

    @Autowired
    private  CreateClientCmd createClientCmd;

    @Autowired
    private  UpdateClientCmd updateClientCmd;

    @Getter
    private  Integer clientId;

    @Before
    public  void createClient(){
       createClientCmd.execute();
       String clientResult = createClientCmd.getResult();
       clientId = createClientCmd.getClientId();
       Assert.assertNotNull(clientResult);
       Assert.assertNotNull(clientId);
    }

    @Test
    public  void  updateClient(){
        updateClientCmd.setClientId(clientId);
        updateClientCmd.execute();
        String updateResult = updateClientCmd.getResult();
        Assert.assertNotNull(updateResult);

    }


}
