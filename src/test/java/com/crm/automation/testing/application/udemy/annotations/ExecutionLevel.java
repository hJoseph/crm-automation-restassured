package com.crm.automation.testing.application.udemy.annotations;

/**
 * @Autor Henry Joseph Calani A.
 **/
public enum ExecutionLevel {
    SYSTEM,
    HIGH,
    MIDDLE,
    LOW,
    REQUIRED,
    FIRST_MASTER,
    SECOND_MASTER,
    THIRD_MASTER,
    FIRST_DETAIL,
    SECOND_DETAIL,
    THIRD_DETAIL,
    CUSTOM
}
