package com.crm.automation.testing.application.udemy.model;

import lombok.Data;

import java.util.Date;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Data
public class Person {
    private Long id;

    private String email;

    private String firstName;

    private String lastName;

    private Boolean deleted;

    private Date createdDate;

    private String position;
}
