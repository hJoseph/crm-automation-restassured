package com.crm.automation.testing.application.udemy.model;

import com.crm.automation.testing.application.udemy.commons.Gender;
import lombok.Data;

import java.util.Date;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Data
public class Client {

    private Long id;

    private String email;

    private String firstName;

    private String lastName;

    private Boolean deleted;

    private Gender gender;

    private Date lastPurchase;

    private Date createdDate;

}
