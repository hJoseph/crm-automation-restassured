package com.crm.automation.testing.application.udemy.commons;

/**
 * @Autor Henry Joseph Calani A.
 **/
public enum Gender {
    MALE,
    FEMALE
}
