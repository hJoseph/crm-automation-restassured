package com.crm.automation.testing.application.udemy.input;

import lombok.Data;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Data
public class DetailInput {
    private Long id;
    private Integer totalProducts;
    private Integer totalPrice;
}
