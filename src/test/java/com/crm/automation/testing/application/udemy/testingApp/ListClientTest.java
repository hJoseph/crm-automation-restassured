package com.crm.automation.testing.application.udemy.testingApp;

import com.crm.automation.testing.application.udemy.AbstractTest;
import com.crm.automation.testing.application.udemy.annotations.ExecutionLevel;
import com.crm.automation.testing.application.udemy.annotations.ExecutionPriority;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Autor Henry Joseph Calani A.
 **/
@ExecutionPriority(ExecutionLevel.THIRD_MASTER)
public class ListClientTest extends AbstractTest {
    @Autowired
    private ListClientCmd listClientCmd;

    @Test
    public void listclient(){
        listClientCmd.execute();
        String resultList = listClientCmd.getResult();
        Assert.assertNotNull(resultList);
    }
}
