package com.crm.automation.testing.application.udemy.input;

import com.crm.automation.testing.application.udemy.commons.Gender;
import lombok.Data;

import java.util.Date;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Data
public class ClienteInput {
    private Long id;

    private Boolean deleted;

    private String email;

    private String firstName;

    private Gender gender;

    private String lastName;

    private Date lastPurchase;

    private Date createdDate;
}
