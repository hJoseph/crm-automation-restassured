package com.crm.automation.testing.application.udemy.tools;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Component
@Scope("prototype")
public class StringGenerator {

    public String next(){
        return RandomStringUtils.randomAlphabetic(10);
    }
}
