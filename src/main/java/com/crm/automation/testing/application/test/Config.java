package com.crm.automation.testing.application.test;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Configuration
@ComponentScan("com.crm.automation.testing.application.test")
public class Config {

}
